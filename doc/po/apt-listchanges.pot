# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the apt-listchanges package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: apt-listchanges 4.6\n"
"Report-Msgid-Bugs-To: apt-listchanges@packages.debian.org\n"
"POT-Creation-Date: 2024-09-22 05:39-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <refentry><refmeta><refentrytitle>
#: apt-listchanges.xml:25
msgid "apt-listchanges"
msgstr ""

#. type: Content of: <refentry><refmeta><manvolnum>
#: apt-listchanges.xml:26
msgid "1"
msgstr ""

#. type: Content of: <refentry><refnamediv><refpurpose>
#: apt-listchanges.xml:31
msgid "Show new changelog entries from Debian package archives"
msgstr ""

#. type: Content of: <refentry><refsynopsisdiv><cmdsynopsis>
#: apt-listchanges.xml:35
msgid ""
"<command>apt-listchanges</command> <group choice=\"opt\"> <arg "
"rep=\"repeat\"><replaceable>options</replaceable></arg> </group> <group "
"choice=\"req\"> <arg><option>--apt</option></arg> <arg "
"rep=\"repeat\"><replaceable>package.deb</replaceable></arg> </group>"
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:48
msgid "DESCRIPTION"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:50
msgid ""
"<command>apt-listchanges</command> is a tool to show what has been changed "
"in a new version of a Debian package, as compared to the version currently "
"installed on the system."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:55
msgid ""
"It does this by extracting the relevant entries from both the NEWS.Debian "
"and changelog<optional>.Debian</optional> files, usually found in "
"<filename>/usr/share/doc/</filename><replaceable>package</replaceable>, from "
"Debian package archives."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:61
msgid ""
"Please note that in the default installation if "
"<command>apt-listchanges</command> is run during upgrades as an APT plugin, "
"it displays NEWS.Debian entries only. This can be changed with the "
"<option>--which</option> option."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:67
msgid ""
"If changelog entries are displayed and the "
"<replaceable>package</replaceable> does not contain "
"changelog<optional>.Debian</optional> file, "
"<command>apt-listchanges</command> calls <command>apt-get "
"changelog</command> command to download the changelog from network. This "
"behavior can be disabled with the <option>--no-network</option> option."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:76
msgid ""
"Given a set of filenames as arguments (or read from apt when using "
"<option>--apt</option>), <command>apt-listchanges</command> will scan the "
"files (assumed to be Debian package archives) for the relevant changelog "
"entries, and display them all in a summary grouped by source package.  The "
"groups are sorted by the urgency of the most urgent change, and then by the "
"package name.  Changes within each package group are displayed in the order "
"of their appearance in the changelog files, i.e. starting from the latest to "
"the oldest; the <option>--reverse</option> option can be used to alter this "
"order."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:91
msgid "OPTIONS"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:93
msgid ""
"<command>apt-listchanges</command> provides the following options to control "
"its behavior. Most of them have their equivalent entries in the "
"configuration file, see the \"CONFIGURATION FILE\" below for details."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:101
msgid "<option>--apt</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:103
msgid ""
"Read filenames from a specially-formatted pipeline (as provided by apt), "
"rather than from command line arguments, and honor certain apt-specific "
"options in the config file.  This pipeline must be in \"version 2\" format, "
"specified in the apt configuration."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:112
msgid "<option>-v, --verbose</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:114
msgid ""
"Display additional (usually unwanted) information.  For instance, print a "
"message when a package of the same or older version is to be installed, or "
"when a package is to be newly installed."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:122
msgid "<option>-f, --frontend</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:125
msgid ""
"Select which frontend to use to display information to the user.  Current "
"frontends include:"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:130
msgid "pager"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:133
msgid ""
"Uses <citerefentry><refentrytitle>sensible-pager</refentrytitle> "
"<manvolnum>1</manvolnum></citerefentry> command to display output.  The "
"command uses PAGER environment variable to choose your favourite pager.  The "
"\"pager\" option may be specified in the configuration file to select a "
"specific pager for use with apt-listchanges."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:144
msgid "browser"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:147
msgid ""
"Displays an HTML-formatted changelog with hyperlinks for bugs and email "
"addresses using the <citerefentry> "
"<refentrytitle>sensible-browser</refentrytitle><manvolnum>1</manvolnum> "
"</citerefentry> command that examines BROWSER environment variable to choose "
"your favourite browser.  The \"browser\" option may be specified in the "
"configuration file to select a specific browser for use with "
"apt-listchanges."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:159
msgid "xterm-pager"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:162
msgid ""
"Uses your favorite pager to display output, but does so in an xterm (using "
"the x-terminal-emulator alternative) in the background.  This allows you to "
"go on with the upgrade if you like, and continue to browse the changelogs.  "
"You can override the terminal emulator to be used with the \"xterm\" "
"configuration option."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:174
msgid "xterm-browser"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:177
msgid ""
"The logical combination of xterm-pager and browser.  Only appropriate for "
"text-mode browsers."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:184
msgid "text"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:187
msgid "Dumps output to stdout, with no pauses."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:193
msgid "syslog"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:196
msgid "Dumps output to syslog.  Disabling the titled option is recommended."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:203
msgid "log"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:206
msgid ""
"Appends output to a log file, with an optional filter process.  Disabling "
"the titled option is recommended."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:214
msgid "mail"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:217
msgid ""
"Sends mail to the address specified with --email-address, and does not "
"display changelogs."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:224
msgid "gtk"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:227
msgid ""
"Spawns a gtk window to display the changelogs. Needs python3-gi to be "
"installed."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><term>
#: apt-listchanges.xml:234
msgid "none"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:237
msgid ""
"Does nothing.  Can be used to prevent apt-listchanges from running when "
"configured to run automatically from apt."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:245
msgid ""
"Please note that apt-listchanges will try to switch to an unprivileged user "
"before spawning commands in \"browser\", \"xterm-browser\", and "
"\"xterm-pager\" frontends.  However this currently does not apply to the "
"\"pager\" frontend.  See also \"ENVIRONMENT VARIABLES\" below."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:254
msgid "<option>--hide</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:257
msgid ""
"For frontends which support it (currently only gtk), hide the window by "
"default."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:264
msgid "<option>--email-address=<replaceable>address</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:267
msgid ""
"In addition to displaying it, mail a copy of the changelog data to the "
"specified address.  To only mail changelog entries, use this option with the "
"special frontend 'mail'."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:275
msgid "<option>--email-format={text|html}</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:278
msgid ""
"If sending mail copies is enabled (see <option>--email-address</option> "
"above), this option selects whether the mail should be sent as an old good "
"plain text data (which is the default behavior), or as html data with "
"clickable links, which might be more convenient for people using graphical "
"mail clients."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:288
msgid "<option>-c, --confirm</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:291
msgid ""
"Once changelogs have been displayed, ask the user whether or not to "
"proceed.  If the user chooses not to proceed, a nonzero exit status will be "
"returned, and apt will abort."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:299
msgid "<option>-a, --show-all</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:302
msgid ""
"Rather than trying to display changelog entries that are newer than the "
"currently installed version of the package, simply display all changelog "
"entries for all packages.  This is useful for viewing the entire changelog "
"of a .deb before extracting it."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:312
msgid "<option>-n, --no-network</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:315
msgid ""
"In rare cases when a binary package (or to be more precise: none of the "
"binary packages built from the same source package that are processed "
"together as a group) does not contain a changelog file, "
"<command>apt-listchanges</command> by default executes <command>apt-get "
"changelog</command> to download changelogs from the network servers usually "
"provided by your operating system distribution.  This option will disable "
"this behavior, what might be useful for example for systems behind a "
"firewall."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:329
msgid "<option>--save-seen=<replaceable>file</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:332
msgid ""
"This option will cause apt-listchanges to keep track of the last version of "
"a package for which changelogs have been displayed, to avoid redisplaying "
"the same changelogs in a future invocation.  The database is stored in the "
"named file.  Specify 'none' to disable this feature."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:342
msgid "<option>--dump-seen</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:345
msgid ""
"Display the contents of the seen database to standard output as a list of "
"lines consisting of source package name and its latest seen version, "
"separated by space.  This option requires the path to the seen database to "
"be known: please either specify it using <option>--save-seen</option> option "
"or pass <option>--profile=apt</option> option to have it read from the "
"configuration file."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:357
msgid "<option>--since=<replaceable>version</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:360
msgid ""
"This option will cause apt-listchanges to show the entries later than the "
"specified version. With this option, the only other argument you can pass is "
"the path to a .deb file."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:368
msgid "<option>--latest=<replaceable>N</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:371
msgid ""
"This option will cause apt-listchanges to show only the latest "
"<replaceable>N</replaceable> entries."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:378
msgid "<option>--which={news|changelogs|both}</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:381
msgid ""
"This option selects whether news (from NEWS.Debian et al.), changelogs (from "
"changelog.Debian et al.) or both should be displayed. The default is to "
"display news when running as an APT plugin, or both otherwise."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:390
msgid "<option>--help</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:393
msgid "Displays syntax information."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:399
msgid "<option>-h, --headers</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:402
msgid ""
"These options will cause apt-listchanges to insert a header before each "
"package's changelog showing the name of the package, and the names of the "
"binary packages which are being upgraded (if there is more than one, or it "
"differs from the source package name)."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:412
msgid "<option>--debug</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:414
msgid "Display some debugging information."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:420
msgid "<option>--profile=<replaceable>name</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:422
msgid ""
"Select an option profile.  <replaceable>name</replaceable> corresponds to a "
"section in <filename>/etc/apt/listchanges.conf</filename>.  The default when "
"invoked from apt is \"apt\", and \"cmdline\" otherwise."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:431
msgid "<option>--log=<replaceable>file</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:434
msgid ""
"Select the file appended to by the log frontend.  The default is "
"<filename>/var/log/apt/listchanges.log</filename>.  The filter command "
"option can be used to modify the output before it is appended to the log "
"file.  Please ensure that you setup log rotation for this file."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:444
msgid "<option>--filter=<replaceable>command</replaceable></option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:447
msgid ""
"Select the command used to filter output before it is appended to the log "
"file by the log frontend.  stdin will receive the "
"<command>apt-listchanges</command> output and stdout will be appended to the "
"log file.  Separate arguments with spaces and quote arguments containing "
"spaces.  The command will not be run using the shell unless the shell is "
"included in the command: <command>sh -c 'date ; cat'</command>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:462
msgid "<option>--reverse</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:465
msgid "Show the changelog entries in reverse order."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:471
msgid ""
"<option>--ignore-apt-assume</option>, "
"<option>--ignore-debian-frontend</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:474
msgid ""
"Disable forcing non-interactive frontend in some of the cases described in "
"the \"AUTOMATIC FRONTEND OVERRIDE\" section below."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:481
msgid "<option>--titled</option>, <option>--untitled</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:484
msgid "Enable or disable the title at the beginning of the output."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:490
msgid "<option>--select-frontend</option>"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:493
msgid ""
"Choose frontend interactively.  This option is mainly for testing purposes, "
"please do not use it."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:504
msgid "AUTOMATIC FRONTEND OVERRIDE"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:506
msgid ""
"For a better integration with existing package management tools, "
"<command>apt-listchanges</command> tries to detect if package upgrades are "
"done in a non-interactive way, and automatically switches its frontend to "
"'text' when <emphasis>any</emphasis> of the following conditions is "
"satisfied:"
msgstr ""

#. type: Content of: <refentry><refsect1><para><itemizedlist><listitem><para>
#: apt-listchanges.xml:512
msgid "the standard output is not connected to terminal;"
msgstr ""

#. type: Content of: <refentry><refsect1><para><itemizedlist><listitem><para>
#: apt-listchanges.xml:515
msgid ""
"the <option>--quiet</option> (<option>-q</option>) option is given to "
"<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum> "
"</citerefentry> (or <citerefentry><refentrytitle>aptitude</refentrytitle> "
"<manvolnum>8</manvolnum></citerefentry>); note however that when the option "
"is used more than once, apt-listchanges switches the frontend to 'mail';"
msgstr ""

#. type: Content of: <refentry><refsect1><para><itemizedlist><listitem><para>
#: apt-listchanges.xml:522
msgid ""
"the <option>--assume-yes</option> (<option>-y</option>) option is given to "
"<citerefentry><refentrytitle>apt-get</refentrytitle> "
"<manvolnum>8</manvolnum></citerefentry>;"
msgstr ""

#. type: Content of: <refentry><refsect1><para><itemizedlist><listitem><para>
#: apt-listchanges.xml:527
msgid ""
"the <envar>DEBIAN_FRONTEND</envar> environment variable is set to "
"\"noninteractive\", and <envar>APT_LISTCHANGES_FRONTEND</envar> is not set."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:533
msgid ""
"For backward compatibility purposes the last two of the above checks can be "
"disabled either with \"ignore_apt_assume=true\" or "
"\"ignore_debian_frontend=true\" configuration file entries (see "
"\"CONFIGURATION FILE\" below) or by using the command line options: "
"<option>--ignore-apt-assume</option> or "
"<option>--ignore-debian-frontend</option>."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:539
msgid ""
"Please also note that the \"mail\" frontend is already non-interactive one, "
"so it is never switched to the \"text\" frontend."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:543
msgid ""
"Additionally <command>apt-listchanges</command> overrides X11-based "
"frontends (\"gtk\", \"xterm-pager\", \"xterm-browser\") with \"pager\" (or "
"\"browser\" in case of \"xterm-browser\") when the environment variable "
"<envar>DISPLAY</envar> is not set."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:549
msgid ""
"Please note that these silent frontends are not subject to the overrides: "
"syslog, log."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:555
msgid "CONFIGURATION FILE"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:557
msgid ""
"<command>apt-listchanges</command> reads its configuration from "
"<filename>/etc/apt/listchanges.conf</filename>.  The file consists of "
"<replaceable>sections</replaceable> with names enclosed in square brackets.  "
"Each section should contain lines in the "
"<replaceable>key</replaceable>=<replaceable>value</replaceable> format.  "
"Lines starting with the \"#\" sign are treated as comments and ignored.  "
"Files named <filename><replaceable>name</replaceable>.conf</filename> in the "
"<filename>/etc/apt/listchanges.conf.d</filename> directory are also read in "
"the same way and override values set in the main configuration file."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:569
msgid ""
"<replaceable>Section</replaceable> is a name of profile that can be used as "
"parameter of the <option>--profile</option> option."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:573
msgid ""
"The configuration of the \"apt\" section can be managed by "
"<citerefentry><refentrytitle>debconf</refentrytitle><manvolnum>7</manvolnum></citerefentry>, "
"and most of the settings there can be changed with the help of the "
"<command>dpkg-reconfigure apt-listchanges</command> command."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:579
msgid ""
"<replaceable>Key</replaceable> is a name of some command-line option (except "
"for <option>--apt</option>, <option>--profile</option>, "
"<option>--help</option>) with the initial hyphens removed, and the remaining "
"hyphens translated to underscores, for example: \"email_format\" or "
"\"save_seen\"."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:586
msgid ""
"<replaceable>Value</replaceable> represents the value of the corresponding "
"option.  For command-line options that do not take argument, like "
"\"confirm\" or \"headers\", the <replaceable>value</replaceable> should be "
"set either to \"1\", \"yes\", \"true\", and \"on\" in order to enable the "
"option, or to \"0\", \"no\", \"false\", and \"off\" to disable it."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:593
msgid ""
"Additionally <replaceable>key</replaceable> can be one of the following "
"keywords: \"browser\", \"pager\" or \"xterm\".  The "
"<replaceable>value</replaceable> of such configuration entry should be the "
"name of an appropriate command, eventually followed by its arguments, for "
"example: \"pager=less -R\"."
msgstr ""

#. type: Content of: <refentry><refsect1><example><title>
#: apt-listchanges.xml:600
msgid "Example configuration file"
msgstr ""

#. type: Content of: <refentry><refsect1><example><programlisting>
#: apt-listchanges.xml:602
#, no-wrap
msgid ""
"[cmdline]\n"
"frontend=pager\n"
"\n"
"[apt]\n"
"frontend=xterm-pager\n"
"email_address=root\n"
"confirm=1\n"
"\n"
"[custom]\n"
"frontend=browser\n"
"browser=mozilla\n"
msgstr ""

#. type: Content of: <refentry><refsect1><example>
#: apt-listchanges.xml:601
msgid "<placeholder type=\"programlisting\" id=\"0\"/>"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:616
msgid ""
"The above configuration file specifies that in command-line mode, the "
"default frontend is \"pager\".  In apt mode, the xterm-pager frontend is "
"default, a copy of the changelogs (if any) should be emailed to root, and "
"apt-listchanges should ask for confirmation.  If apt-listchanges is invoked "
"with --profile=custom, the browser frontend will be used, and invoke "
"mozilla."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:626
msgid "ENVIRONMENT"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:630
msgid "APT_LISTCHANGES_FRONTEND"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:631
msgid "Frontend to use."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:637
msgid "APT_LISTCHANGES_USER, SUDO_USER, USERNAME"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:638
msgid ""
"The value of the first existing of the above variables will be used as the "
"name of user to switch to when running commands spawned by the \"browser\", "
"\"xterm-browser\", and \"xterm-pager\" frontends if "
"<command>apt-listchanges</command> is started by a privileged user."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:648
msgid "DEBIAN_FRONTEND"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:649
msgid ""
"If set to \"noninteractive\", then it can force "
"<command>apt-listchanges</command> to use non-interactive frontend, see the "
"\"AUTOMATIC FRONTEND OVERRIDE\" section for details."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:658
msgid "BROWSER"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:659
msgid ""
"Used by the browser frontend, should be set to a command expecting a file: "
"URL for an HTML file to display."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:668
msgid "PAGER"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:669
msgid "Used by the pager frontend."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:675
msgid "APT_HOOK_INFO_FD"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:676
msgid ""
"File descriptor to read package names from in the <option>--apt</option> "
"mode.  (Apt is expected to set this variable to a proper file descriptor "
"number)."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:684
msgid "SEEN DATABASE INITIALIZATION"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:687
msgid ""
"When <command>apt-listchanges</command> is installed for the first time or "
"upgraded from an old version that did not use the current seen database "
"format, it enables a <command>systemd</command> timer, "
"<command>apt-listchanges.timer</command>, which attempts hourly to activate "
"<command>apt-listchanges.service</command>, which scans the changelog and "
"NEWS files of all installed packages and uses their contents to populate the "
"seen database."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:697
msgid ""
"Pre-populating the database like this makes "
"<command>apt-listchanges</command> run faster because it then doesn't have "
"to parse the changelog and NEWS files of currently installed packages during "
"upgrades when determining which new entries to display."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:704
msgid ""
"Pre-populating the database should only need to be done once on any given "
"host, since from that point forward <command>apt-listchanges</command> "
"updates the database automatically during upgrades. Therefore, after the "
"service runs successfully to completion, the timer is automatically "
"disabled."
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:711
msgid ""
"If for some reason you believe the <command>apt-listchanges</command> seen "
"database is incomplete or inaccurate, you can rebuild it by removing "
"<filename>/var/lib/apt/listchanges</filename> and then executing "
"<command>systemctl start apt-listchanges.service</command>. Note that this "
"runs to completion in the foreground."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:721
msgid "FILES"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:725
msgid "/etc/apt/listchanges.conf"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:726
msgid "Configuration file."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:730
msgid "/etc/apt/listchanges.conf.d/*.conf"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:731
msgid "Configuration file override files."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:735
msgid "/etc/apt/apt.conf.d/20listchanges"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:736
msgid "File used for registering apt-listchanges into apt system."
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: apt-listchanges.xml:740
msgid "/var/lib/apt/listchanges"
msgstr ""

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: apt-listchanges.xml:741
msgid "Database used for save-seen."
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:747
msgid "AUTHOR"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:750
msgid "apt-listchanges was written by Matt Zimmerman &lt;mdz@debian.org&gt;"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:754
msgid "The current maintainer is Jonathan Kamens &lt;jik@kamens.us&gt;"
msgstr ""

#. type: Content of: <refentry><refsect1><title>
#: apt-listchanges.xml:760
msgid "SEE ALSO"
msgstr ""

#. type: Content of: <refentry><refsect1><para>
#: apt-listchanges.xml:763
msgid ""
"<citerefentry><refentrytitle>sensible-pager</refentrytitle><manvolnum>1</manvolnum></citerefentry>, "
"<citerefentry><refentrytitle>sensible-browser</refentrytitle><manvolnum>1</manvolnum></citerefentry>, "
"<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum></citerefentry>, "
"<citerefentry><refentrytitle>aptitude</refentrytitle><manvolnum>8</manvolnum></citerefentry>"
msgstr ""
